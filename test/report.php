<?php
// Initialize the session
session_start();
include "config.php"; // Using database connection file here

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true || $_SESSION["roles"] !== 'Reporter')
{
    header("location: login.php");
    exit;
}
		
if(isset($_POST['submit']))
{		
    $title = $_POST['title'];
	$keyword = $_POST['keyword'];
    $description = $_POST['description'];
	$username = $_SESSION["username"];

    $insert = mysqli_query($link,"INSERT INTO `bugreport`(`title`, `keyword`, `description`, `status`, `username`) VALUES ('$title', '$keyword','$description', 'Open', '$username')");
    echo "Bug report raised!<br />";
	
    if(!$insert)
    {
        echo mysqli_error();
    }
}

mysqli_close($link); // Close and secure connection
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Raise Bug Report</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <div class="page-header">
	    <h1>Raise bug report</h1>
    </div>
    <p>
	<form method="POST">
  Title : <input type="text" name="title" placeholder="Title" size="50" Required>
  <br/>
  Keyword : <input type="text" name="keyword" placeholder="Keyword" size="50" Required>
  <br/>
  Description : <input type="text" name="description" placeholder="Description" size="50" Required>
  <br/>
  <br/>
  <input type="submit" name="submit" class="btn btn-primary" value="Submit">
   <input type="reset" class="btn btn-default" value="Reset">  
  <a href="welcome.php" class="btn btn-danger">Back to Home</a>
</form>

    </p>
</body>
</html>